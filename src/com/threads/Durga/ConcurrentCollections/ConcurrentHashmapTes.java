package com.threads.Durga.ConcurrentCollections;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashmapTes {


	public static void main(String[] args) {
		Map <String,String> lMap = new ConcurrentHashMap<String,String>();
		lMap.put("A","NEW ENTRY");
		lMap.put("B","NEW ENTRY");
		lMap.put("C","NEW ENTRY");
		
		Iterator<Entry<String, String>> lIterate = lMap.entrySet().iterator();
		for(int i=0;i<10;i++){
			AddMap lAddMap = new AddMap(lMap);
			Thread thread= new Thread(lAddMap ,"TASK-"+i);
			thread.start();
		}
		while(lIterate.hasNext()){
			System.out.println(lIterate.next());
			try {
				/**
				 * The sleep method is added intentionally to 
				 * create a delay so that we get concurrent modification Exception
				 */
				Thread.currentThread().sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}

class AddMap implements Runnable{
	Map <String,String> lMap;
	public AddMap(Map <String,String> lMap) {
		// TODO Auto-generated constructor stub
		this.lMap = lMap;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		lMap.put(Thread.currentThread().getName(),"ADDED THREAD");
	}
	
}