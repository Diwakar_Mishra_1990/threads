package com.threads.Durga.ConcurrentCollections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConcurrentModificationExceptionExample {

	public static void main(String[] args) {
		List <String> lList = new ArrayList<String>();
		lList.add("A");
		lList.add("B");
		lList.add("C");
		
		Iterator<String> lIterate = lList.listIterator();
		for(int i=0;i<10;i++){
			AddList lAddList = new AddList(lList);
			Thread thread= new Thread(lAddList ,"TASK-"+i);
			thread.start();
		}
		while(lIterate.hasNext()){
			System.out.println(lIterate.next());
			try {
				/**
				 * The sleep method is added intentionally to 
				 * create a delay so that we get concurrent modification Exception
				 */
				Thread.currentThread().sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}

class AddList implements Runnable{
	List <String> lList;
	public AddList(List <String> lList) {
		// TODO Auto-generated constructor stub
		this.lList = lList;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		lList.add(Thread.currentThread().getName());
	}
	
}