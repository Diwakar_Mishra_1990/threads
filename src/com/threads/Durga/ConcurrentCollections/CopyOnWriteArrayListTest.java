package com.threads.Durga.ConcurrentCollections;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

public class CopyOnWriteArrayListTest {

	public static void main(String[] args) {
		List <String> lList = new java.util.concurrent.CopyOnWriteArrayList<String>();
		lList.add("A");
		lList.add("B");
		lList.add("C");
		
		Iterator<String> lIterate = lList.listIterator();
		for(int i=0;i<10;i++){
			AddCopyOnWriteArrayList lAddCopyOnWriteArrayList = new AddCopyOnWriteArrayList(lList);
			Thread thread= new Thread(lAddCopyOnWriteArrayList ,"TASK-"+i);
			thread.start();
		}
		while(lIterate.hasNext()){
			System.out.println(lIterate.next());
			try {
				/**
				 * The sleep method is added intentionally to 
				 * create a delay so that we get concurrent modification Exception
				 */
				Thread.currentThread().sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}

class AddCopyOnWriteArrayList implements Runnable{
	List <String> lList;
	public AddCopyOnWriteArrayList(List <String> lList) {
		// TODO Auto-generated constructor stub
		this.lList = lList;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("ADDED LIST");
		lList.add(Thread.currentThread().getName());
		System.out.println(lList);
	}
	
}