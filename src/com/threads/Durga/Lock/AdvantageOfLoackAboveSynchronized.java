package com.threads.Durga.Lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Diwakar Mishra It is widely said that synchronized is a blocking
 *         operation. Any thread which attempts for getting the lock is blocked
 *         and it has to wait for acquiring thread to release the lock . However
 *         with lock we have a method called Try Lock. It helps us perform
 *         another task in caseif we dont get the lock . It is like . A Thread
 *         tells if I dont get the lock of the critical section . I will not
 *         execute critical section. I would do some other task. This basically
 *         optimizes our action at time of concurrency . The only difference
 *         from previous example is that instead of using <lock_instance>.lock()
 *         .We use lock.tryLock(). This assures that iF the critical section is
 *         open lock.trylock will help us get the lock other wise immediately we
 *         will get false immediately, unlike the synchronized way where even
 *         though we dont get the lock we are forced to sit idle to get the lock
 *         of the critical section.
 *
 */
public class AdvantageOfLoackAboveSynchronized {
	public static void main(String[] args) {
		LockThread1 lThreadRun = new LockThread1();
		for(int i=0;i<10;i++) {
			Thread t = new Thread(lThreadRun);
			t.start();
		}
	}
}

class LockThread1 implements Runnable {
	ReentrantLocLaboratory1 lLockLab = new ReentrantLocLaboratory1();

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lLockLab.method1();
	}

}

/**
 * 
 * @author Diwakar Mishra In this class we will be using locks in a heuristic
 *         manner in order to show what basically lock is. Here w have used lock
 *         .tryLock() So the difference with Class WhyLockReEntrant would be
 *         that all 9 threads wont produce output . Like was the previous case
 */
class ReentrantLocLaboratory1 {
	ReentrantLock lReentrantLock = new ReentrantLock();

	public void method1() {
		// lReentrantLock.lock();
		// instead of this we put lock.tryLock()
		if (lReentrantLock.tryLock()) {
			try {

				System.out.println("############################################################");
				System.out.println("LOCK ACQUIRED BY  THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK ACQUIRED BY  THREAD" + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				method2();
			} catch (Exception e) {
				System.out.println("EXCEPTION CAUGHT");
			} finally {
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("############################################################");
				lReentrantLock.unlock();
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
			}
		}

	}

	public void method2() {
		if (lReentrantLock.tryLock()) {
			try {
				System.out.println("LOCK ACQUIRED BY  THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK ACQUIRED BY  THREAD" + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				method3();
			} catch (Exception e) {
				System.out.println("EXCEPTION CAUGHT");
			} finally {
				lReentrantLock.unlock();
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
			}
		}
	}

	public void method3() {
		if (lReentrantLock.tryLock()) {
			try {
				lReentrantLock.lock();
				System.out.println("LOCK ACQUIRED BY  THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK ACQUIRED BY  THREAD" + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				lReentrantLock.lock();
				System.out.println("LOCK ACQUIRED BY  THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK ACQUIRED BY  THREAD" + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				lReentrantLock.lock();
				System.out.println("LOCK ACQUIRED BY  THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK ACQUIRED BY  THREAD" + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
			} catch (Exception e) {
				System.out.println("EXCEPTION CAUGHT");
			} finally {
				lReentrantLock.unlock();
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				lReentrantLock.unlock();
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				lReentrantLock.unlock();
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				System.out.println("IS LOCK ACQUIRED : " + lReentrantLock.isLocked());
				
				/**
				 * Extra UNLOCK ADDED FOR THE IF CONDITION
				 * 
				 */
				System.out.println("*************SPECIFICALLY ADDED FOR LOCK>TRYLOCK");
				lReentrantLock.unlock();
				System.out.println("LOCK RELEASED BY THREAD " + Thread.currentThread().getName());
				System.out.println("LOCK LEFT BY THREAD " + Thread.currentThread().getName() + "FOR TIMES "
						+ lReentrantLock.getHoldCount());
				
			}
		}

	}

}