package com.threads.Durga.Lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class AdvantagesOfReEntrantLockWithTimedWaiting {
	public static void main(String[] args) {
		Resource lTestLockWaitTime = new Resource();
		SampleJob lJob = new SampleJob(lTestLockWaitTime);
		SampleJob lJob1 = new SampleJob(lTestLockWaitTime);
		Thread t = new Thread(lJob, "JOB1");
		Thread t1 = new Thread(lJob, "JOB2");
		t.start();
		t1.start();
	}
}

class SampleJob implements Runnable {
	Resource lTestLockWaitTime;

	public SampleJob(Resource lTestLockWaitTime) {
		// TODO Auto-generated constructor stub
		this.lTestLockWaitTime = lTestLockWaitTime;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lTestLockWaitTime.reEntatinTimedWait();
	}

}

class Resource {

	ReentrantLock lLock = new ReentrantLock();

	@SuppressWarnings("finally")
	public void reEntatinTimedWait() {
		try {
			do {
				if (lLock.tryLock(5000, TimeUnit.MILLISECONDS)) {
					System.out.println("THREAD IS LOCKED by " + Thread.currentThread().getName());
					try {
						Thread.sleep(30000);
						System.out.println("RELEASING LOCK");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						lLock.unlock();
						break;
					}

				} else {
					System.out.println(
							"UNABLE TO RELEASE LOCK  THREAD " + Thread.currentThread().getName() + " NEEDS TO WAIT");
				}
			} while (true);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
