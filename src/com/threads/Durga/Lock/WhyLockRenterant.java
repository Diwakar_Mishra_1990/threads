package com.threads.Durga.Lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Diwakar Mishra The basic question here is that why are we calling
 *         locks to be reentrant The main reason for the lock to be reentrant is
 *         that if a thread gets control of the lock it can reenter the
 *         protected block again and again The reason being lock basically has a
 *         counter implying how many times the lock is being applied by the
 *         thread The lock is considered release only when the counter present
 *         inside the lock reaches zero
 * Lock is consistent across all the methods. and result is regular
 */
public class WhyLockRenterant {

	public static void main(String[] args) {
		LockThread lThreadRun = new LockThread();
		for(int i=0;i<10;i++) {
			Thread t = new Thread(lThreadRun);
			t.start();
		}
	}
}

class LockThread implements Runnable {
	ReentrantLocLaboratory lLockLab =new ReentrantLocLaboratory();
	@Override
	public void run() {
		// TODO Auto-generated method stub
	   	lLockLab.method1();
	}
	
}
/**
 * 
 * @author Diwakar Mishra In this class we will be using locks in a heuristic
 *         manner in order to show what basically lock is
 */
class ReentrantLocLaboratory {
	ReentrantLock lReentrantLock = new ReentrantLock();
	
	public void method1() {
		try {
		lReentrantLock.lock();
		System.out.println("############################################################");
		System.out.println("LOCK ACQUIRED BY  THREAD "+ Thread.currentThread().getName());
		System.out.println("LOCK ACQUIRED BY  THREAD"+Thread.currentThread().getName()
				             +"FOR TIMES "+lReentrantLock.getHoldCount());
		System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		method2();
		} catch(Exception e){
			System.out.println("EXCEPTION CAUGHT");
		}
		finally {
			System.out.println("LOCK RELEASED BY THREAD "+ Thread.currentThread().getName());
			System.out.println("LOCK LEFT BY THREAD "+Thread.currentThread().getName()
		             +"FOR TIMES "+lReentrantLock.getHoldCount());
			System.out.println("############################################################");
			lReentrantLock.unlock();
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		}
	}
	public void method2() {
		try {
		lReentrantLock.lock();
		System.out.println("LOCK ACQUIRED BY  THREAD "+ Thread.currentThread().getName());
		System.out.println("LOCK ACQUIRED BY  THREAD"+Thread.currentThread().getName()
				             +"FOR TIMES "+lReentrantLock.getHoldCount());
		System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		method3();
		} catch(Exception e){
			System.out.println("EXCEPTION CAUGHT");
		}
		finally {
			lReentrantLock.unlock();
			System.out.println("LOCK RELEASED BY THREAD "+ Thread.currentThread().getName());
			System.out.println("LOCK LEFT BY THREAD "+Thread.currentThread().getName()
		             +"FOR TIMES "+lReentrantLock.getHoldCount());
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		}
	}
	public void method3() {
		try {
		lReentrantLock.lock();
		System.out.println("LOCK ACQUIRED BY  THREAD "+ Thread.currentThread().getName());
		System.out.println("LOCK ACQUIRED BY  THREAD"+Thread.currentThread().getName()
				             +"FOR TIMES "+lReentrantLock.getHoldCount());
		System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		lReentrantLock.lock();
		System.out.println("LOCK ACQUIRED BY  THREAD "+ Thread.currentThread().getName());
		System.out.println("LOCK ACQUIRED BY  THREAD"+Thread.currentThread().getName()
				             +"FOR TIMES "+lReentrantLock.getHoldCount());
		System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		lReentrantLock.lock();
		System.out.println("LOCK ACQUIRED BY  THREAD "+ Thread.currentThread().getName());
		System.out.println("LOCK ACQUIRED BY  THREAD"+Thread.currentThread().getName()
				             +"FOR TIMES "+lReentrantLock.getHoldCount());
		System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		} catch(Exception e){
			System.out.println("EXCEPTION CAUGHT");
		}
		finally {
			lReentrantLock.unlock();
			System.out.println("LOCK RELEASED BY THREAD "+ Thread.currentThread().getName());
			System.out.println("LOCK LEFT BY THREAD "+Thread.currentThread().getName()
		             +"FOR TIMES "+lReentrantLock.getHoldCount());
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
			lReentrantLock.unlock();
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
			System.out.println("LOCK RELEASED BY THREAD "+ Thread.currentThread().getName());
			System.out.println("LOCK LEFT BY THREAD "+Thread.currentThread().getName()
		             +"FOR TIMES "+lReentrantLock.getHoldCount());
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
			lReentrantLock.unlock();
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
			System.out.println("LOCK RELEASED BY THREAD "+ Thread.currentThread().getName());
			System.out.println("LOCK LEFT BY THREAD "+Thread.currentThread().getName()
		             +"FOR TIMES "+lReentrantLock.getHoldCount());
			System.out.println("IS LOCK ACQUIRED : "+lReentrantLock.isLocked());
		}
	}
	
}