package com.threads.Durga.ThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPool {
	public static void main(String[] args) {
		/**
		 * Here we have kept the thread pool size as 3
		 */
		ExecutorService service = Executors.newFixedThreadPool(3);
		ExecutorJob []jobs = new ExecutorJob[]{
				new ExecutorJob("ADAM"),
				new ExecutorJob("EVE"),
				new ExecutorJob("MIKE"),
				new ExecutorJob("JEREMY"),
				new ExecutorJob("MONTY"),
				new ExecutorJob("RAYMOND"),
				new ExecutorJob("ALEX"),
				new ExecutorJob("JOHN"),
				new ExecutorJob("RIKI"),
				new ExecutorJob("JAMES"),
		};
		for(ExecutorJob job: jobs){
			service.submit(job);
		}
		service.shutdown();
	}
}

class ExecutorJob implements Runnable {

	String name;
	public ExecutorJob(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(this.name+ " JOB STARTED BY :::::::"+Thread.currentThread().getName());
		try {
			/**
			 * The sleeping of a thread is made intentional in oder to show at a time
			 * if no of jobs is greater than size of the pool then the job has to wait
			 * to get the free thread
			 * This sleep also helps us in displaying hat at max at any time no of jobs
			 * completed in thread pool is same as size of the thread pool
			 */
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(this.name+" JOB COMPLETED BY :::::::"+Thread.currentThread().getName());
	}

}