package com.threads.Durga.ThreadPool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureWithExector {
	public static void main(String[] args) {
		ExecutorService service = Executors.newFixedThreadPool(3);
		FutureJob []jobs  = new FutureJob[] {
				new FutureJob(10),
				new FutureJob(20),
				new FutureJob(30),
				new FutureJob(40),
				new FutureJob(50),
				new FutureJob(60),
				new FutureJob(70),
				new FutureJob(80),
				new FutureJob(90),
				new FutureJob(100)
		};
		
		for(FutureJob job: jobs) {
			Future <Integer> result = service.submit(job);
			try {
				System.out.println(result.get());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		service.shutdown();
	}
}

class FutureJob implements Callable<Integer> {

	private int endInteger;

	public FutureJob(int endInteger) {
		// TODO Auto-generated constructor stub
		this.endInteger = endInteger;
	}

	@Override
	public Integer call() throws Exception {
		// TODO Auto-generated method stub
		int sum = 0;
		for (int i = 0; i < this.endInteger; i++) {
			sum += i;
		}
		return sum;

	}

}