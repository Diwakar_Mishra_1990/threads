package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 
 * @author Diwakar Mishra
 *
 *         So what are the benefits of using a cached thread pool ? Well, the
 *         docs state them as follows:
 * 
 *         The pool creates new threads if needed but reuses previously
 *         constructed threads if they are available.
 * 
 *         Cached thread pool helps improve the performance of applications that
 *         make many short-lived asynchronous tasks.
 * 
 *         Only if no threads are available for reuse will a new thread be
 *         created and added to the pool.
 * 
 *         Threads that have not been used for more than sixty seconds are
 *         terminated and removed from the cache. Hence a pool which has not
 *         been used long enough will not consume any resources.
 * 
 *         So without further ado let�s have a look at obtaining an
 *         ExecutorService instance and use it to run a few threads. The below
 *         line of code will get you a cached thread pool that is ready to roll.
 */
public class CachedThreadPool {

	public static void main(String[] args) {
		Server server = new Server();
		for (int i = 0; i < 10; i++) {
			Task task = new Task("TASK-" + i);
			server.executetask(task);
		}
		server.endServer();
	}
}

class Server {
	private ThreadPoolExecutor lExecutor;

	public Server() {
		// TODO Auto-generated constructor stub
		lExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	}

	public void executetask(Runnable task) {
		System.out.printf("Server: A new task has arrived\n");
		lExecutor.execute(task);
		System.out.printf("Server: Pool Size: %d\n", lExecutor.getPoolSize());
		System.out.printf("Server: Active Count: %d\n", lExecutor.getActiveCount());
		System.out.printf("Server: Completed Tasks: %d\n", lExecutor.getCompletedTaskCount());
	}

	public void endServer() {
		lExecutor.shutdown();
	}
}

class Task implements Runnable {
	private Date initDate;
	private String name;

	public Task(String name) {
		initDate = new Date();
		this.name = name;
	}

	@Override
	public void run() {
		System.out.printf("%s: Task %s: Created on: %s\n", Thread.currentThread().getName(), name, initDate);
		System.out.printf("%s: Task %s: Started on: %s\n", Thread.currentThread().getName(), name, new Date());
		try {
			System.out.printf("%s: Task %s: Doing a task during %d seconds\n", Thread.currentThread().getName(), name,
					5000);
			/**
			 * The sleep is added intentionally to check the effect of size of
			 * tread pool on the execution of thread
			 */
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
