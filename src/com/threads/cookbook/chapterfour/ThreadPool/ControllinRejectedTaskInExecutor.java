package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

public class ControllinRejectedTaskInExecutor {

	public static void main(String[] args) {
		ThreadPoolExecutor lExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		RejectedTaskController controller = new RejectedTaskController();
		lExecutor.setRejectedExecutionHandler(controller);
		System.out.printf("Main: Starting.\n");
		for (int i = 0; i < 3; i++) {
			Task task = new Task("Task" + i);
			lExecutor.submit(task);
		}
		System.out.printf("Main: Shutting down the Executor.\n");
		lExecutor.shutdown();
		/**
		 * Following threads would be rejected as shutdown is alreaduy
		 * instructed to the executor. Now Rejected controller will take in charge
		 * 
		 */
		for (int i = 0; i < 3; i++) {
			Task task = new Task("Task" + (3+i));
			lExecutor.submit(task);
		}
	}
}

/**
 * 
 * @author Diwakar Mishra This method handles all the rejected tasks
 * 
 *         When you want to finish the execution of an executor, you use the
 *         shutdown() method to indicate that it should finish. The executor
 *         waits for the completion of the tasks that are running or waiting for
 *         their execution, and then finishes its execution. If you send a task
 *         to an executor between the shutdown() method and the end of its
 *         execution, the task is rejected, because the executor no longer
 *         accepts new tasks. The ThreadPoolExecutor class provides a mechanism,
 *         which is called when a task is rejected.
 * 
 */
class RejectedTaskController implements RejectedExecutionHandler {

	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		System.out.printf("RejectedTaskController: The task %s has been rejected\n", r.toString());
		System.out.printf("RejectedTaskController: %s\n", executor.toString());
		System.out.printf("RejectedTaskController: Terminating:%s\n", executor.isTerminating());
		System.out.printf("RejectedTaksController: Terminated:%s\n", executor.isTerminated());
	}
}

class RejectedTask implements Runnable {

	private String name;

	public RejectedTask(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Task " + this.name + ": Starting");
		try {
			System.out.printf("Task %s: ReportGenerator: Generating an report during %d seconds\n", name, 5000);
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Task %s: Ending\n", name);
	}

	public String toString() {
		return name;
	}

}