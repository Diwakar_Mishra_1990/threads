package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Diwakar Mishra Creates and executes a periodic action that becomes
 *         enabled first after the given initial delay, and subsequently with
 *         the given period; that is executions will commence after initialDelay
 *         then initialDelay+period, then initialDelay + 2 * period, and so on.
 *         If any execution of the task encounters an exception, subsequent
 *         executions are suppressed. Otherwise, the task will only terminate
 *         via cancellation or termination of the executor. If any execution of
 *         this task takes longer than its period, then subsequent executions
 *         may start late, but will not concurrently execute.
 */
public class RunningTaskInExecutorPeriodically {
	public static void main(String[] args) {
		ScheduledExecutorService lExecutor = Executors.newScheduledThreadPool(1);
		TaskPeriodical task = new TaskPeriodical("Task");
		/**
		 * Creates and executes a periodic action that becomes enabled first
		 * after the given initial delay, and subsequently with the given
		 * period; that is executions will commence after initialDelay then
		 * initialDelay+period, then initialDelay + 2 * period, and so on. If
		 * any execution of the task encounters an exception, subsequent
		 * executions are suppressed. Otherwise, the task will only terminate
		 * via cancellation or termination of the executor. If any execution of
		 * this task takes longer than its period, then subsequent executions
		 * may start late, but will not concurrently execute. Parameters:
		 * command :the task to execute initialDelay :the time to delay first
		 * execution period: the period between successive executions unit: the
		 * time unit of the initialDelay and period parameters Returns:
		 * ScheduledFuture representing pending completion of the task, and
		 * whose get() method will throw an exception upon cancellation
		 */
		ScheduledFuture<?> result = lExecutor.scheduleAtFixedRate(task, 1, 2, TimeUnit.SECONDS);
		for (int i = 0; i < 20; i++) {
			System.out.printf("Main: Delay: %d\n", result.getDelay(TimeUnit.MILLISECONDS));
			// Sleep the thread during 500 milliseconds.
			try {
				TimeUnit.MILLISECONDS.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		lExecutor.shutdown();
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Main: Finished at: %s\n", new Date());
	}
}

/**
 * 
 * @author Diwakar Mishra When you want to execute a periodic task using the
 *         Executor framework, you need a ScheduledExecutorService object. To
 *         create it (as with every executor), Java recommends the use of the
 *         Executors class. This class works as a factory of executor objects.
 *         In this case, you should use the newScheduledThreadPool() method to
 *         create a ScheduledExecutorService object. That method receives as a
 *         parameter the number of threads of the pool. As you have only one
 *         task in this example, you have passed the value 1 as a parameter.
 *         Once you have the executor needed to execute a periodic task, you
 *         send the task to the executor. You have used the
 *         scheduledAtFixedRate() method. This method accepts four parameters:
 *         the task you want to execute periodically, the delay of time until
 *         the first execution of the task, the period between two executions,
 *         and the time unit of the second and third parameters. It's a constant
 *         of the TimeUnit class. The TimeUnit class is an enumeration with the
 *         following constants: DAYS, HOURS, MICROSECONDS, MILLISECONDS,
 *         MINUTES, NANOSECONDS, and SECONDS. An important point to consider is
 *         that the period between two executions is the period of time between
 *         these two executions that begins. If you have a periodic task that
 *         takes 5 sceconds to execute and you put a period of 3 seconds, you
 *         will have two instances of the task executing at a time. The method
 *         scheduleAtFixedRate() returns a ScheduledFuture object, which extends
 *         the Future interface, with methods to work with scheduled tasks.
 *         ScheduledFuture is a parameterized interface. In this example, as
 *         your task is a Runnable object that is not parameterized, you have to
 *         parameterize them with the ? symbol as a parameter. You have used one
 *         method of the ScheduledFuture interface. The getDelay() method
 *         returns the time until the next execution of the task. This method
 *         receives a TimeUnit constant with the time unit in which you want to
 *         receive the results.
 */
class TaskPeriodical implements Runnable {
	private String name;

	public TaskPeriodical(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		System.out.printf("%s: Starting at : %s\n", name, new Date());
	}
}