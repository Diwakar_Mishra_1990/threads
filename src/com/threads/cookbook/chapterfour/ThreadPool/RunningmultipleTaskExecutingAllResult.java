package com.threads.cookbook.chapterfour.ThreadPool;
		
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 
 * @author Diwakar Mishra This is here used to show the use of invoke all
 */
public class RunningmultipleTaskExecutingAllResult {
	public static void main(String[] args) throws ExecutionException {
		ThreadPoolExecutor lExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		List<TaskInvokeAny> lTasks = new ArrayList<TaskInvokeAny>();
		for (int i = 0; i < 10; i++) {
			TaskInvokeAny lTask = new TaskInvokeAny("TASK-" + i);
			lTasks.add(lTask);
		}
		try {
			/**
			 * In this recipe, you have learned how to send a list of tasks to
			 * an executor and wait for the finalization of all of them using
			 * the invokeAll() method. This method receives a list of the
			 * Callable objects and returns a list of the Future objects. This
			 * list will have a Future object per task in the list. The first
			 * object in the list of the Future objects will be the object that
			 * controls the first task in the list of the Callable objects, and
			 * so on. The first point to take into consideration is that the
			 * type of data used for the parameterization of the Future
			 * interface in the declaration of the list that stores the result
			 * objects must be compatible with the one used to parameterized the
			 * Callable objects. In this case, you have used the same type of
			 * data: the Result class. Another important point about the
			 * invokeAll() method is that you will use the Future objects only
			 * to get the results of the tasks. As the method finishes when all
			 * the tasks have finished, if you call the isDone() method of the
			 * Future objects that is returned, all the calls will return the
			 * true value
			 */
			List<Future<String>> results = lExecutor.invokeAll(lTasks);
			for(Future<String> lResult : results ) {
				System.out.println(lResult.get());
			}
		} catch (InterruptedException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lExecutor.shutdown();
	}

}

/**
 * 
 * @author Diwakar Mishra In this recipe, you have learned how to send a list of
 *         tasks to an executor and wait for the finalization of all of them
 *         using the invokeAll() method. This method receives a list of the
 *         Callable objects and returns a list of the Future objects. This list
 *         will have a Future object per task in the list. The first object in
 *         the list of the Future objects will be the object that controls the
 *         first task in the list of the Callable objects, and so on. The first
 *         point to take into consideration is that the type of data used for
 *         the parameterization of the Future interface in the declaration of
 *         the list that stores the result objects must be compatible with the
 *         one used to parameterized the Callable objects. In this case, you
 *         have used the same type of data: the Result class. Another important
 *         point about the invokeAll() method is that you will use the Future
 *         objects only to get the results of the tasks. As the method finishes
 *         when all the tasks have finished, if you call the isDone() method of
 *         the Future objects that is returned, all the calls will return the
 *         true value.
 */
class TaskInvokeAll implements Callable<String> {
	private Date initDate;
	private String name;

	public TaskInvokeAll(String name) {
		initDate = new Date();
		this.name = name;
	}

	@Override
	public String call() throws InterruptedException {
		System.out.printf("%s: Task %s: Created on: %s\n", Thread.currentThread().getName(), name, initDate);
		System.out.printf("%s: Task %s: Started on: %s\n", Thread.currentThread().getName(), name, new Date());
		System.out.printf("%s: Task %s: Doing a task during %d seconds\n", Thread.currentThread().getName(), name,
				5000);
		/**
		 * The sleep is added intentionally r=to check the effect of size of
		 * tread pool on the execution of thread
		 */
		Thread.sleep(5000);
		return Thread.currentThread().getName() + "::::::::::" + this.name;
	}
}
