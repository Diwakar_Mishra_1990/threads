package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 
 * @author Diwakar Mishra A common problem in concurrent programming is when you
 *         have various concurrent tasks that solve a problem, and you are only
 *         interested in the first result of those tasks. For example, you want
 *         to sort an array. You have various sort algorithms. You can launch
 *         all of them and get the result of the first one that sorts these,
 *         that is, the fastest sorting algorithm for a given array. In this
 *         recipe, you will learn how to implement this scenario using the
 *         ThreadPoolExecutor class. You are going to implement an example where
 *         a user can be validated by two mechanisms. The user will be validated
 *         if one of those mechanisms validates it.
 */
public class RunningmultipleTaskExecutingFirstResult {

	public static void main(String[] args) {
		ThreadPoolExecutor lExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		List<TaskInvokeAny> lTasks = new ArrayList<TaskInvokeAny>();
		for (int i = 0; i < 10; i++) {
			TaskInvokeAny lTask = new TaskInvokeAny("TASK-" + i);
			lTasks.add(lTask);
		}
		try {
			/**
			 * So, we have two tasks that can return the true value or throw an
			 * Exception exception. You can have the following four
			 * possibilities: 
			 * A. Both tasks return the true value. The result of
			 * the invokeAny() method is the name of the task that finishes in
			 * the first place. 
			 * B. The first task returns the true value and the
			 * second one throws Exception. The result of the invokeAny() method
			 * is the name of the first task. 
			 * C. The first task throws Exception and the second one returns the true value. 
			 * The result of the invokeAny() method is the name of the second task. 
			 * D. Both tasks throw Exception. In that class, the invokeAny() method throws an
			 * ExecutionException exception.
			 */
			String result = lExecutor.invokeAny(lTasks);
			System.out.println(result);
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lExecutor.shutdown();
	}

}

/**
 * 
 * @author Diwakar Mishra Invoke Any only takes callable because it expects you
 *         to return result . The invokeAny() method of the ThreadPoolExecutor
 *         class receives a list of tasks, launches them, and returns the result
 *         of the first task that finishes without throwing an exception. This
 *         method returns the same data type that the call() method of the tasks
 *         you launch returns. In this case, it returns a String value.
 */
class TaskInvokeAny implements Callable<String> {
	private Date initDate;
	private String name;

	public TaskInvokeAny(String name) {
		initDate = new Date();
		this.name = name;
	}

	@Override
	public String call() throws InterruptedException {
		System.out.printf("%s: Task %s: Created on: %s\n", Thread.currentThread().getName(), name, initDate);
		System.out.printf("%s: Task %s: Started on: %s\n", Thread.currentThread().getName(), name, new Date());
		System.out.printf("%s: Task %s: Doing a task during %d seconds\n", Thread.currentThread().getName(), name,
				5000);
		/**
		 * The sleep is added intentionally r=to check the effect of size of
		 * tread pool on the execution of thread
		 */
		Thread.sleep(5000);
		return Thread.currentThread().getName() + "::::::::::" + this.name;
	}
}
