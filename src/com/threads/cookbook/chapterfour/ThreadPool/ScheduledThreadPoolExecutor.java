package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Diwakar Mishra Scheduled thread pool executor schedules the star of
 *         the thread with a delay. As with class ThreadPoolExecutor, to create
 *         a scheduled executor, Java recommends the utilization of the
 *         Executors class. In this case, you have to use the
 *         newScheduledThreadPool() method. You have passed the number 1 as a
 *         parameter to this method. This parameter is the number of threads you
 *         want to have in the pool.
 */

/**
 * 
 * @author Diwakar Mishra You can also use the Runnable interface to implement
 *         the tasks, because the schedule() method of the
 *         ScheduledThreadPoolExecutor class accepts both types of tasks.
 *         Although the ScheduledThreadPoolExecutor class is a child class of
 *         the ThreadPoolExecutor class and, therefore, inherits all its
 *         features, Java recommends the utilization of
 *         ScheduledThreadPoolExecutor only for scheduled tasks. Finally, you
 *         can configure the behavior of the ScheduledThreadPoolExecutor class
 *         when you call the shutdown() method and there are pending tasks
 *         waiting for the end of their delay time. The default behavior is that
 *         those tasks will be executed despite the finalization of the
 *         executor.
 */
public class ScheduledThreadPoolExecutor {
	public static void main(String[] args) {
		/**
		 * Here we have scheduled Exeuctor Service instead of normal Executor
		 * Service Or Thread PoolExecutor
		 */
		ScheduledExecutorService lExecutor = Executors.newScheduledThreadPool(3);
		List<TaskInvokeAny> lTasks = new ArrayList<TaskInvokeAny>();
		for (int i = 0; i < 10; i++) {
			TaskSchedule task = new TaskSchedule("Task " + i);
			/**
			 * For working with scheduled executor we nned to make sure that we
			 * use the command schedule which is equivalent to submit and
			 * schedule The second parameter is the delay
			 */
			/**
			 * To execute a task in this scheduled executor after a period of
			 * time, you have to use the schedule() method. This method receives
			 * the following three parameters: 
			 * A. The task you want to execute
			 * B. The period of time you want the task to wait before its
			 * execution 
			 * C. The unit of the period of time, specified as a
			 * constant of the TimeUnit class
			 */
			lExecutor.schedule(task, 10 * i, TimeUnit.SECONDS);
		}
		/**
		 * One thing must be kept in mind that a shutdown method call in
		 * executor does not terminate all the operations of the thread. It
		 * simply says that once the thread execution is completed in thread
		 * poolit renders the thread pool unusable
		 */
		lExecutor.shutdown();
		
		try {
			lExecutor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

/**
 * 
 * @author Diwakar Mishra Invoke Any only takes callable because it expects you
 *         to return result . The invokeAny() method of the ThreadPoolExecutor
 *         class receives a list of tasks, launches them, and returns the result
 *         of the first task that finishes without throwing an exception. This
 *         method returns the same data type that the call() method of the tasks
 *         you launch returns. In this case, it returns a String value.
 */
class TaskSchedule implements Callable<String> {
	private Date initDate;
	private String name;

	public TaskSchedule(String name) {
		initDate = new Date();
		this.name = name;
	}

	@Override
	public String call() throws InterruptedException {
		System.out.printf("%s: Task %s: Created on: %s\n", Thread.currentThread().getName(), name, initDate);
		System.out.printf("%s: Task %s: Started on: %s\n", Thread.currentThread().getName(), name, new Date());
		System.out.printf("%s: Task %s: Doing a task\n", Thread.currentThread().getName(), name);
		return Thread.currentThread().getName() + "::::::::::" + this.name;
	}
}
