package com.threads.cookbook.chapterfour.ThreadPool;

import java.util.concurrent.Callable;

/**
 * @author Diwakar Mishra
 *
 *         Normally, when you execute concurrent tasks using an executor, you
 *         will send Runnable or Callable tasks to the executor and get Future
 *         objects to control the method.
 * 
 *         You can find situations, where you need to send the tasks to the
 *         executor in one object and process the results in another one. For
 *         such situations, the Java Concurrency API provides the
 *         CompletionService class. This CompletionService class has a method to
 *         send the tasks to an executor and a method to get the Future object
 *         for the next task that has finished its execution.
 * 
 *         Internally, it uses an Executor object to execute the tasks. This
 *         behavior has the advantage to share a CompletionService object, and
 *         sends tasks to the executor so the others can process the results.
 *         The limitation is that the second object can only get the Future
 *         objects for those tasks that have finished its execution, so these
 *         Future objects can only be used to get the results of the tasks.
 */
public class SeparatingLaunchingAndResultProcessing {

}

class ReportGeneratorProcessingResult implements Callable<String> {

	private String sender;
	private String title;

	public ReportGeneratorProcessingResult(String sender, String title) {
		this.sender = sender;
		this.title = title;
	}
	@Override
	public String call() throws Exception {
		try {
			System.out.printf("%s_%s: ReportGenerator: Generating a report during %d seconds\n", this.sender,
					this.title, 5000);
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return sender+"::::::::::::::::::::::::::::"+title;
	}

}