package com.threads.cookbook.chaptersevwn.CustomizingConcurrencyClasses;

import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CusoizingThreadPoolExecutor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

/**
 * The Executor framework is a mechanism that allows you to separate the thread
 * creation from its execution. It's based on the Executor and ExecutorService
 * interfaces with the ThreadPoolExecutor class that implements both interfaces.
 * It has an internal pool of threads and provides methods that allow you to
 * send two kinds of tasks for their execution in the pooled threads. These
 * tasks are: A. The Runnable interface to implement tasks that don't return a
 * result B. The Callable interface to implement tasks that return a result In
 * both cases, you only send the task to the executor. The executor uses one of
 * its pooled threads or creates a new one to execute those tasks. The executor
 * also decides the moment in which the task is executed. In this recipe, you
 * will learn how to override some methods of the ThreadPoolExecutor class to
 * calculate the execution time of the tasks that you execute in the executor
 * and to write in the console statistics about the executor when it completes
 * its execution.
 * 
 */
class MyExecutor extends ThreadPoolExecutor {
	private ConcurrentHashMap<String, Date> startTimes;

	public MyExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
		startTimes = new ConcurrentHashMap<>();
	}
}