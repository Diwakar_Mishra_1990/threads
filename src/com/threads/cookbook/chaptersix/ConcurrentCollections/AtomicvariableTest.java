package com.threads.cookbook.chaptersix.ConcurrentCollections;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 
 * @author Diwakar Mishra
 *
 *         Atomic variables were introduced in Java Version 5 to provide atomic
 *         operations on single variables. When you work with a normal variable,
 *         each operation that you implement in Java is transformed in several
 *         instructions that is understandable by the machine when you compile
 *         the program. For example, when you assign a value to a variable, you
 *         only use one instruction in Java, but when you compile this program,
 *         this instruction is transformed in various instructions in the JVM
 *         language. This fact can provide data inconsistency errors when you
 *         work with multiple threads that share a variable. To avoid these
 *         problems, Java introduced the atomic variables. When a thread is
 *         doing an operation with an atomic variable, if other threads want to
 *         do an operation with the same variable, the implementation of the
 *         class includes a mechanism to check that the operation is done in one
 *         step. Basically, the operation gets the value of the variable,
 *         changes the value in a local variable, and then tries to change the
 *         old value for the new one. If the old value is still the same, it
 *         does the change. If not, the method begins the operation again. This
 *         operation is called Compare and Set.
 */
public class AtomicvariableTest {
	public static void main(String[] args) {
		Account account = new Account();
		account.setBalance(1000);
		Company company = new Company(account);
		Thread companyThread = new Thread(company);
		Bank bank = new Bank(account);
		Thread bankThread = new Thread(bank);
		System.out.printf("Account : Initial Balance: %d\n", account.getBalance());
		companyThread.start();
		bankThread.start();
		try {
			companyThread.join();
			bankThread.join();
			System.out.printf("Account : Final Balance: %d\n", account.getBalance());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class Account {
	/**
	 * Atomic is always a number
	 * To test this jut do one thing . Remove AtomicLong to Long
	 */
	private AtomicLong balance;

	public Account() {
		balance = new AtomicLong();
	}

	public long getBalance() {
		return balance.get();
	}

	public void setBalance(long balance) {
		this.balance.set(balance);
	}

	public void addAmount(long amount) {
		this.balance.getAndAdd(amount);
	}

	public void subtractAmount(long amount) {
		this.balance.getAndAdd(-amount);
	}
}

class Company implements Runnable {
	private Account account;

	public Company(Account account) {
		this.account = account;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			account.addAmount(1000);
		}
	}
}

class Bank implements Runnable {

	private Account account;

	public Bank(Account account) {
		this.account = account;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			account.subtractAmount(1000);
		}
	}

}