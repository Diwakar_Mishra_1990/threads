package com.threads.cookbook.chaptersix.ConcurrentCollections;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 
 * @author Diwakar Mishra Non BLocking Thread Safe List
 */
public class ConcurrentLinkedDequeue {
	public static void main(String[] args) {
		ConcurrentLinkedDeque<String> list = new ConcurrentLinkedDeque<>();
		for (int i = 0; i < 10; i++) {
			AddTask task = new AddTask(list);
			Thread threads = new Thread(task, "ADD TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d AddTask threads have been launched\n", 10);
		System.out.println(list);
		for (int i = 0; i < 10; i++) {
			PollTask task = new PollTask(list);
			Thread threads = new Thread(task, "POLL TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d PollTask threads have been launched\n", 10);
		System.out.println(list);
	}
}

class AddTask implements Runnable {

	private ConcurrentLinkedDeque<String> list;

	public AddTask(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		String name = Thread.currentThread().getName();
		for (int i = 0; i < 10; i++) {
			list.add(name + ": Element " + i);
		}
		System.out.println(list);
	}

}

class PollTask implements Runnable {
	private ConcurrentLinkedDeque<String> list;

	public PollTask(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			list.pollFirst();
			list.pollLast();
		}
		System.out.println(list);
	}

}