package com.threads.cookbook.chaptersix.ConcurrentCollections;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 
 * @author Diwakar Mishra Using blocking thread-safe lists
 */
public class LinkedBlockingDeque {

	public static void main(String[] args) {
		ConcurrentLinkedDeque<String> list = new ConcurrentLinkedDeque<>();
		for (int i = 0; i < 10; i++) {
			AddTaskLinkedDequeue task = new AddTaskLinkedDequeue(list);
			Thread threads = new Thread(task, "ADD TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d AddTaskLinkedDequeue threads have been launched\n", 10);
		System.out.println(list);
		for (int i = 0; i < 10; i++) {
			PollTaskLinkedDequeue task = new PollTaskLinkedDequeue(list);
			Thread threads = new Thread(task, "POLL TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d PollTaskLinkedDequeue threads have been launched\n", 10);
		System.out.println(list);
	}
}

class AddTaskLinkedDequeue implements Runnable {

	private ConcurrentLinkedDeque<String> list;

	public AddTaskLinkedDequeue(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		String name = Thread.currentThread().getName();
		for (int i = 0; i < 10; i++) {
			list.add(name + ": Element " + i);
		}
		System.out.println(list);
	}

}

class PollTaskLinkedDequeue implements Runnable {
	private ConcurrentLinkedDeque<String> list;

	public PollTaskLinkedDequeue(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			list.pollFirst();
			list.pollLast();
		}
		System.out.println(list);
	}

}