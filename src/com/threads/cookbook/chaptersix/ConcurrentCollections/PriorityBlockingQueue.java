package com.threads.cookbook.chaptersix.ConcurrentCollections;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 
 * @author Diwakar Mishra Using blocking thread-safe lists ordered by priority
 * 
 *         Note: String always implements comparator
 * 
 * 
 *         A typical need when you work with data structures is to have an
 *         ordered list. Java provides PriorityBlockingQueue that has this
 *         functionality. All the elements you want to add to
 *         PriorityBlockingQueue have to implement the Comparable interface.
 *         This interface has a method, compareTo() that receives an object of
 *         the same type, so you have two objects to compare: the one that is
 *         executing the method and the one that is received as a parameter. The
 *         method must return a number less than zero if the local object is
 *         less than the parameter, a number bigger that zero if the local
 *         object is greater than the parameter, and the number zero if both
 *         objects are equal. PriorityBlockingQueue uses the compareTo() method
 *         when you insert an element in it to determine the position of the
 *         element inserted. The greater elements will be the tail of the queue.
 *         Another important characteristic of PriorityBlockingQueue is that
 *         it's a blocking data structure. It has methods that, if they can't do
 *         their operation immediately, block the thread until they can do it.
 */
public class PriorityBlockingQueue {
	public static void main(String[] args) {
		ConcurrentLinkedDeque<String> list = new ConcurrentLinkedDeque<>();
		for (int i = 0; i < 10; i++) {
			AddTaskPriority task = new AddTaskPriority(list);
			Thread threads = new Thread(task, "ADD TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d AddTaskPriority threads have been launched\n", 10);
		System.out.println(list);
		for (int i = 0; i < 10; i++) {
			PollTaskPriority task = new PollTaskPriority(list);
			Thread threads = new Thread(task, "POLL TASK-" + i);
			threads.start();
		}
		System.out.printf("Main: %d PollTaskPriority threads have been launched\n", 10);
		System.out.println(list);
	}
}

class AddTaskPriority implements Runnable {

	private ConcurrentLinkedDeque<String> list;

	public AddTaskPriority(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		String name = Thread.currentThread().getName();
		for (int i = 0; i < 10; i++) {
			list.add(name + ": Element " + i);
		}
		System.out.println(list);
	}

}

class PollTaskPriority implements Runnable {
	private ConcurrentLinkedDeque<String> list;

	public PollTaskPriority(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			list.pollFirst();
			list.pollLast();
		}
		System.out.println(list);
	}

}