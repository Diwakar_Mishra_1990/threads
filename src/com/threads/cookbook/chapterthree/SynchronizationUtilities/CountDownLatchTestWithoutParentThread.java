package com.threads.cookbook.chapterthree.SynchronizationUtilities;

import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author Diwakar Mishra
 *
 *         The Java concurrency API provides a class that allows one or more
 *         threads to wait until a set of operations are made. It's the
 *         CountDownLatch class. This class is initialized with an integer
 *         number, which is the number of operations the threads are going to
 *         wait for. When a thread wants to wait for the execution of these
 *         operations, it uses the await() method. This method puts the thread
 *         to sleep until the operations are completed. When one of these
 *         operations finishes, it uses the countDown() method to decrement the
 *         internal counter of the CountDownLatch class. When the counter
 *         arrives to 0, the class wakes up all the threads that were sleeping
 *         in the await() method.
 * 
 * 
 * 
 *         So in operation it is just the reverse of semaphore in semaphore you
 *         initialize the number to to the semaphore which is the number of
 *         synchronized thread that can access the critical section
 *         simulaneously
 * 
 *         In count down latch you assign a number to the latch The latch will
 *         make sure that the thread waits until it the required number of
 *         thread comes which is equal to the number assigned to the latch . If
 *         that is met then we are going to process the next statement . Perfect
 *         example is videoconferencing.
 */
public class CountDownLatchTestWithoutParentThread {
	public static void main(String[] args) {
		VideoconferenceWithoutThread conference = new VideoconferenceWithoutThread(10);
		for (int i = 0; i < 10; i++) {
			Paticipant2 participate = new Paticipant2(conference, "Participant " + i);
			Thread t = new Thread(participate);
			t.start();
		}
	}
}

/**
 * 
 * @author Diwakar Mishra
 *
 *         We have intentionally created a thread to show that a thread is
 *         waiting until all the participant has come . Even if you remove this
 *         runnable still it will work
 */
class VideoconferenceWithoutThread {

	private final CountDownLatch controller;

	public VideoconferenceWithoutThread(int number) {
		controller = new CountDownLatch(number);
	}

	public void arrive(String name) {
		System.out.printf("%s has arrived. \n", name);
		controller.countDown();
		try {
			controller.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.printf("VideoConference: All the participants have come\n");
		System.out.printf("VideoConference: Let's start...\n");
	}
}

class Paticipant2 implements Runnable {

	private VideoconferenceWithoutThread lVideoConference;
	private String name;

	public Paticipant2(VideoconferenceWithoutThread lVideoConference, String name) {
		this.lVideoConference = lVideoConference;
		this.name = name;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			/**
			 * Sleep is added intentionally to make sure
			 * that participant thread is created
			 */
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lVideoConference.arrive(name);
	}
}
