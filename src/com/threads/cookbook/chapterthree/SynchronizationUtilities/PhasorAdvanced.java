package com.threads.cookbook.chapterthree.SynchronizationUtilities;

import java.util.Date;
import java.util.concurrent.Phaser;

/**
 * 
 * @author Diwakar Mishra This exercise simulates the realization of an exam
 *         that has three exercises. All the students have to finish one
 *         exercise before they can start the next one. To implement this
 *         synchronization requirement, we use the Phaser class, but you have
 *         implemented your own phaser extending the original class to override
 *         the onAdvance() method.
 */
public class PhasorAdvanced {
	public static void main(String[] args) {
		MyPhaser phaser = new MyPhaser();
		Student students[] = new Student[5];
		for (int i = 0; i < students.length; i++) {
			students[i] = new Student(phaser);
			phaser.register();
		}
		Thread threads[] = new Thread[students.length];
		for (int i = 0; i < students.length; i++) {
			threads[i] = new Thread(students[i], "Student " + i);
			threads[i].start();
		}
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.printf("Main: The phaser has finished: %s.\n", phaser.isTerminated());
	}
}

class MyPhaser extends Phaser {

	/**
	 * Override the onAdvance() method. According to the value of the phase
	 * attribute, we call a different auxiliary method. If the phase is equal to
	 * zero, you have to call the studentsArrived() method. If the phase is
	 * equal to one, you have to call the finishFirstExercise() method. If the
	 * phase is equal to two, you have to call the finishSecondExercise()
	 * method, and if the phase is equal to three, you have to call the
	 * finishExam() method. Otherwise, we return the true value to indicate that
	 * the phaser has terminated.
	 * 
	 * This method is called by the phaser before making a phase change and
	 * before waking up all the threads that were sleeping in the
	 * arriveAndAwaitAdvance() method. This method receives as parameters the
	 * number of the actual phase, where 0 is the number of the first phase and
	 * the number of registered participants. The most useful parameter is the
	 * actual phase. If you execute a different operation depending on the
	 * actual phase, you have to use an alternative structure (if/else or
	 * switch) to select the operation you want to execute. In the example, we
	 * used a switch structure to select a different method for each change of
	 * phase. The onAdvance() method returns a Boolean value that indicates if
	 * the phaser has terminated or not. If the phaser returns a false value, it
	 * indicates that it hasn't terminated, so the threads will continue with
	 * the execution of other phases. If the phaser returns a true value, then
	 * the phaser still wakes up the pending threads, but moves the phaser to
	 * the terminated state, so all the future calls to any method of the phaser
	 * will return immediately, and the isTerminated() method returns the true
	 * value. In the Core class, when you created the MyPhaser object, you
	 * didn't specify the number of participants in the phaser. You made a call
	 * to the register() method for every Student object created to register a
	 * participant in the phaser. This calling doesn't establish a relation
	 * between the Student object or the thread that executes it and the phaser.
	 * Really, the number of participants in a phaser is only a number. There is
	 * no relationship between the phaser and the participants.
	 */
	@Override
	protected boolean onAdvance(int phase, int registeredParties) {
		switch (phase) {
		case 0:
			return studentsArrived();
		case 1:
			return finishFirstExercise();
		case 2:
			return finishSecondExercise();
		case 3:
			return finishExam();
		default:
			return true;
		}
	}

	private boolean studentsArrived() {
		System.out.printf("Phaser: The exam are going to start. The students are ready.\n");
		System.out.printf("Phaser: We have %d students.\n", getRegisteredParties());
		return false;
	}

	private boolean finishFirstExercise() {
		System.out.printf("Phaser: All the students have finished the first exercise.\n");
		System.out.printf("Phaser: It's time for the second one.\n");
		return false;
	}

	private boolean finishSecondExercise() {
		System.out.printf("Phaser: All the students have finished the second exercise.\n");
		System.out.printf("Phaser: It's time for the third one.\n");
		return false;
	}

	private boolean finishExam() {
		System.out.printf("Phaser: All the students have finished the exam.\n");
		System.out.printf("Phaser: Thank you for your time.\n");
		return true;
	}
}

class Student implements Runnable {
	private Phaser phaser;

	public Student(Phaser phaser) {
		this.phaser = phaser;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.printf("%s: Has arrived to do the exam. %s\n", Thread.currentThread().getName(), new Date());
		phaser.arriveAndAwaitAdvance();
		System.out.printf("%s: Is going to do the first exercise.%s\n", Thread.currentThread().getName(), new Date());
		doExercise1();
		System.out.printf("%s: Has done the first exercise. %s\n", Thread.currentThread().getName(), new Date());
		phaser.arriveAndAwaitAdvance();
		System.out.printf("%s: Is going to do the second exercise.%s\n", Thread.currentThread().getName(), new Date());
		doExercise2();
		System.out.printf("%s: Has done the second exercise. %s\n", Thread.currentThread().getName(), new Date());
		phaser.arriveAndAwaitAdvance();
		System.out.printf("%s: Is going to do the third exercise. %s\n", Thread.currentThread().getName(), new Date());
		doExercise3();
		System.out.printf("%s: Has finished the exam. %s\n", Thread.currentThread().getName(), new Date());
		phaser.arriveAndAwaitAdvance();
	}

	private void doExercise1() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void doExercise2() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void doExercise3() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}