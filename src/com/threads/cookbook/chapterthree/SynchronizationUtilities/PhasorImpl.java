package com.threads.cookbook.chapterthree.SynchronizationUtilities;

import java.util.concurrent.Phaser;

/**
 * 
 * @author Diwakar Mishra The program starts creating a Phaser object that will
 *         control the synchronization of the threads at the end of each phase.
 *         The constructor of Phaser receives the number of participants as a
 *         parameter. In our case, Phaser has three participants. This number
 *         indicates to Phaser the number of threads that have to execute an
 *         arriveAndAwaitAdvance() method before Phaser changes the phase and
 *         wakes up the threads that were sleeping.
 * 
 */
public class PhasorImpl {
	public static void main(String[] args) {

		Phaser lPhaser = new Phaser(10);
		PhsorResource lPhsorResource = new PhsorResource(lPhaser);
		for (int i = 0; i < 10; i++) {
			Phasortask ltask = new Phasortask(lPhsorResource);
			Thread lPhaserThread = new Thread(ltask, "TASK-" + i);
			lPhaserThread.start();
		}
		System.out.println("Terminated: " + lPhaser.isTerminated());
	}
}

class Phasortask implements Runnable {

	PhsorResource lPhsorResource;

	Phasortask(PhsorResource lPhsorResource) {
		this.lPhsorResource = lPhsorResource;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lPhsorResource.method1();
	}

}

class PhsorResource {
	Phaser lPhaser;

	public PhsorResource(Phaser lPhaser) {
		// TODO Auto-generated constructor stub
		this.lPhaser = lPhaser;
	}

	public void method1() {
		System.out.println("ENTERED METHOD1 BY THREAD :::::::::::: " + Thread.currentThread().getName());
		lPhaser.arriveAndAwaitAdvance();
		method2();
	}

	public void method2() {
		System.out.println("ENTERED METHOD2 BY THREAD :::::::::::: " + Thread.currentThread().getName());
		lPhaser.arriveAndAwaitAdvance();
		method3();
	}

	public void method3() {
		System.out.println("ENTERED METHOD3 BY THREAD :::::::::::: " + Thread.currentThread().getName());
		lPhaser.arriveAndDeregister();
	}

}
/**
 * object is a call to the arriveAndAwaitAdvance() method of the Phaser object.
 * As we mentioned earlier, the Phaser knows the number of threads that we want
 * to synchronize. When a thread calls this method, Phaser decreases the number
 * of threads that have to finalize the actual phase and puts this thread to
 * sleep until all the remaining threads finish this phase
 * 
 * we used arriveAndDeregister(). This notifies the phaser that this thread has
 * finished the actual phase, but it won't participate in the future phases, so
 * the phaser won't have to wait for it to continue.
 * 
 * there is a call to the arriveAndAwaitAdvance() method of the phaser. With
 * this call, we guarantee that all the threads finish at the same time. When
 * this method ends its execution, there is a call to the arriveAndDeregister()
 * method of the phaser. With this call, we deregister the threads of the phaser
 * as we explained before, so when all the threads finish, the phaser will have
 * zero participants.
 * 
 * Finally, the main() method waits for the completion of the three threads and
 * calls the isTerminated() method of the phaser. When a phaser has zero
 * participants, it enters the so called termination state and this method
 * returns true. As we deregister all the threads of the phaser, it will be in
 * the termination state and this call will print true to the console.
 * 
 * 
 * The Phaser class provides other methods related to the change of phase. These
 * methods are as follows:
 * 
 * A. arrive(): This method notifies the phaser that one participant has
 * finished the actual phase, but it should not wait for the rest of the
 * participants to continue with its execution. Be careful with the utilization
 * of this method, because it doesn't synchronize with other threads.
 * 
 * B. awaitAdvance(int phase): This method puts the current thread to sleep
 * until all the participants of the phaser have finished the current phase of
 * the phaser, if the number we pass as the parameter is equal to the actual
 * phase of the phaser. If the parameter and the actual phase of the phaser
 * aren't equal, the method returns immediately.
 * 
 * C. awaitAdvanceInterruptibly(int phaser): This method is equal to the method
 * explained earlier, but it throws an InterruptedException exception if the
 * thread that is sleeping in this method is interrupted.
 */

/**
 * Registering participants in the Phaser
 * 
 * When you create a Phaser object, you indicate how many participants will have
 * that phaser. But the Phaser class has two methods to increment the number of
 * participants of a phaser. These methods are as follows:
 * 
 * A. register(): This method adds a new participant to Phaser. This new
 * participant will be considered as unarrived to the actual phase.
 * 
 * B. bulkRegister(int Parties): This method adds the specified number of
 * participants to the phaser. These new participants will be considered as
 * unarrived to the actual phase. The only method provided by the Phaser class
 * to decrement the number of participants is the arriveAndDeregister() method
 * that notifies the phaser that the thread has finished the actual phase, and
 * it doesn't want to continue with the phased operation. Forcing the
 * termination of a Phaser When a phaser has zero participants, it enters a
 * state denoted by Termination. The Phaser class provides forceTermination() to
 * change the status of the phaser and makes it enter in the Termination state
 * independently of the number of participants registered in the phaser. This
 * mechanism may be useful when one of the participants has an error situation,
 * to force the termination of the phaser. When a phaser is in the Termination
 * state, the awaitAdvance() and arriveAndAwaitAdvance() methods immediately
 * return a negative number, instead of a positive one that returns normally. If
 * you know that your phaser could be terminated, you should verify the return
 * value of those methods to know if the phaser has been terminated.
 * 
 */