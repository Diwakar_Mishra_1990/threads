package com.threads.cookbook.chapterthree.SynchronizationUtilities;

import java.util.concurrent.Semaphore;

/**
 * @author Diwakar Mishra
 * 
 *         A semaphore is a counter that protects the access to one or more
 *         shared resources.
 * 
 *         When a thread wants to access one of these shared resources, first,
 *         it must acquire the semaphore. If the internal counter of the
 *         semaphore is greater than 0, the semaphore decrements the counter and
 *         allows access to the shared resource. A counter bigger than 0 means
 *         there are free resources that can be used, so the thread can access
 *         and use one of them.
 * 
 * 
 *         Otherwise, if the counter of the semaphore is 0, the semaphore puts
 *         the thread to sleep until the counter is greater than 0. A value of 0
 *         in the counter means all the shared resources are used by other
 *         threads, so the thread that wants to use one of them must wait until
 *         one is free. When the thread has finished the use of the shared
 *         resource, it must release the semaphore so that the other thread can
 *         access the shared resource. That operation increases the internal
 *         counter of the semaphore.
 */
public class SemaphoreTest {

	// Concept of demaphor is nearly ewual to concept of lock
	// except here participatimg thread simultaneously is more than1
	public static void main(String[] args) {
		Semaphore lSemaphore = new Semaphore(3);
		ResourceSemaphore lResourceSemaphore = new ResourceSemaphore(lSemaphore);
		for (int i = 0; i < 10; i++) {
			ResourceSemaphoreTask ltask = new ResourceSemaphoreTask(lResourceSemaphore);
			Thread t = new Thread(ltask, "TASK:::" + i);
			t.start();
		}
	}
}

class ResourceSemaphoreTask implements Runnable {

	ResourceSemaphore lResourceSemaphore;

	public ResourceSemaphoreTask(ResourceSemaphore lResourceSemaphore) {
		// TODO Auto-generated constructor stub
		this.lResourceSemaphore = lResourceSemaphore;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			lResourceSemaphore.printRsourceSemaphoreJob();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/**
 * 
 * @author Diwakar Mishra
 *
 *         The Semaphore class has two additional versions of the acquire()
 *         method: ff acquireUninterruptibly(): The acquire() method; when the
 *         internal counter of the semaphore is 0, blocks the thread until the
 *         semaphore is released. During this blocked time, the thread may be
 *         interrupted and then this method throws an InterruptedException
 *         exception. This version of the acquire operation ignores the
 *         interruption of the thread and doesn't throw any exceptions. ff
 *         tryAcquire(): This method tries to acquire the semaphore. If it can,
 *         the method returns the true value. But if it can't, the method
 *         returns the false value instead of being blocked and waits for the
 *         release of the semaphore. It's your responsibility to take the
 *         correct action based on the return value.
 */
class ResourceSemaphore {
	/**
	 * The Semaphore class has two additional versions of the acquire() method:
	 * 
	 * A.acquireUninterruptibly(): The acquire() method; when the internal
	 * counter of the semaphore is 0, blocks the thread until the semaphore is
	 * released. During this blocked time, the thread may be interrupted and
	 * then this method throws an InterruptedException exception. This version
	 * of the acquire operation ignores the interruption of the thread and
	 * doesn't throw any exceptions.
	 * 
	 * B. tryAcquire(): This method tries to acquire the semaphore. If it can,
	 * the method returns the true value. But if it can't, the method returns
	 * the false value instead of being blocked and waits for the release of the
	 * semaphore. This is similar to tryLock for lock
	 * It's your responsibility to take the correct action based on
	 * the return value.
	 */
	private final Semaphore semaphore;

	public ResourceSemaphore(Semaphore semaphore) {
		// TODO Auto-generated constructor stub
		this.semaphore = semaphore;
	}

	public void printRsourceSemaphoreJob() throws InterruptedException {
		System.out.println("ENTERD THE PRNT RESOURCE METHOD :::::::::: " + Thread.currentThread().getName());
		semaphore.acquire();
		System.out.println("SEMAPHORE ACQUIRED :::::::::: SEMAPHORE COUNT " + Thread.currentThread().getName());
		Thread.sleep(5000);
		System.out.println("SEMAPHORE RELEASED :::::::::: SEMAPHORE COUNT " + Thread.currentThread().getName());
		semaphore.release();
	}
}
