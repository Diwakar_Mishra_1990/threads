package com.threads.cookbook.chaptertwo.synchronize;

public class BankImplmplementation {

	public static void main(String[] args) {
		Bank bank = new Bank();
		BankOperation lBankOperationAdd = new BankOperation("ADD", bank);
		BankOperation lBankOperationSub = new BankOperation("SUB", bank);
		Thread addThread = new Thread(lBankOperationAdd);
		Thread subThread = new Thread(lBankOperationSub);
		addThread.start();
		subThread.start();
	}
}

class Bank {
	int amount = 1000;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	public synchronized void addAmount(int amount) {
		this.amount += amount;
	}
	public synchronized  void subtractAmount(int amount) {
		this.amount -= amount;
	}
}

class BankOperation implements Runnable {

	String operation = "";
	Bank bank;
	public BankOperation(String operation, Bank bank) {
		// TODO Auto-generated constructor stub
		this.bank = bank;
		this.operation = operation;
	    
	}
	@Override
	public void run() {
		System.out.println("Initial Balance : " +bank.getAmount());
		for(int i = 0 ;i<10; i++) {
			if(operation.equals("ADD")){
		        bank.addAmount(100);	
		    }
	        else if (operation.equals("SUB")) {
	        	bank.subtractAmount(100);
	        }
		}
		System.out.println("Final Balance : " +bank.getAmount());
	}
}
