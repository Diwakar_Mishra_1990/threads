package com.threads.cookbook.chaptertwo.synchronize;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MultipleFunctionWithSynchronizedBlock {
	public static void main(String[] args) {
		ReadWriteResourceSyncBlock lResource = new ReadWriteResourceSyncBlock();
		for(int i=0;i<3;i++){
			GetPrice1RunnnableSynchronizedBlock lGetPrice1RunnnableSynchronizedBlock= new GetPrice1RunnnableSynchronizedBlock(lResource);
			Thread t1 = new Thread(lGetPrice1RunnnableSynchronizedBlock,"THREAD-GET_PRICE_1-"+i);
			GetPrice2RunnnableSynchronizedBlock lGetPrice2RunnnableSynchronizedBlock= new GetPrice2RunnnableSynchronizedBlock(lResource);
			Thread t2 = new Thread(lGetPrice2RunnnableSynchronizedBlock,"THREAD-GET_PRICE_2-"+i);
			SetPrice1RunnnableSynchronizedBlock lSetPrice1RunnnableSynchronizedBlock= new SetPrice1RunnnableSynchronizedBlock(lResource, i);
			Thread t3 = new Thread(lSetPrice1RunnnableSynchronizedBlock,"THREAD-SET_PRICE_1-"+i);
			SetPrice2RunnnableSynchronizedBlock lSetPrice2RunnnableSynchronizedBlock= new SetPrice2RunnnableSynchronizedBlock(lResource, i);
			Thread t4 = new Thread(lSetPrice2RunnnableSynchronizedBlock,"THREAD-SET_PRICE_2-"+i);
			t1.start();
			t2.start();
			t3.start();
			t4.start();
		}
	}
}
class GetPrice1RunnnableSynchronizedBlock implements Runnable {
	ReadWriteResourceSyncBlock lResource;

	public GetPrice1RunnnableSynchronizedBlock(ReadWriteResourceSyncBlock lResource2) {
		// TODO Auto-generated constructor stub
		this.lResource = lResource2;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.getPrice1();
	}

}

class GetPrice2RunnnableSynchronizedBlock implements Runnable {
	ReadWriteResourceSyncBlock lResource;

	public GetPrice2RunnnableSynchronizedBlock(ReadWriteResourceSyncBlock lResource2) {
		// TODO Auto-generated constructor stub
		this.lResource = lResource2;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.getPrice2();
	}

}

class SetPrice2RunnnableSynchronizedBlock implements Runnable {
	ReadWriteResourceSyncBlock lResource;
	int price;

	public SetPrice2RunnnableSynchronizedBlock(ReadWriteResourceSyncBlock lResource2, int price) {
		// TODO Auto-generated constructor stub
		this.price = price;
		this.lResource = lResource2;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.setPrice2(this.price);
	}

}
class SetPrice1RunnnableSynchronizedBlock implements Runnable {
	ReadWriteResourceSyncBlock lResource;
	int price;

	public SetPrice1RunnnableSynchronizedBlock(ReadWriteResourceSyncBlock lResource2, int price) {
		// TODO Auto-generated constructor stub
		this.price = price;
		this.lResource = lResource2;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.setPrice1(this.price);
	}

}

class ReadWriteResourceSyncBlock {
	/**
	 * This is all together a different issue here Here we are using
	 * ReadWriteLock not Lock which is different
	 */

	ReadWriteLock lReentrantReadWriteLock = new ReentrantReadWriteLock();

	public  int getPrice1() {
		synchronized (this) {
			System.out.println("ENTERED THE METHOD TO GET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			try {
				/**
				 * The sleep time of 10 seconds have been added intentionally to
				 * show that read lock and write lock can coexist and it is not
				 * like lock where if in one method if lock is acquired we wont
				 * be able to get lock in different method Thats not the case
				 * with read write lock . read lock is a different lock and
				 * write lock is a different lock Consider like these are two
				 * different objects used to lock critical section
				 */
				Thread.currentThread().sleep(25000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				System.out.println("LEFT THE METHOD TO GET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			}
			return price1;
		}

	}

	public  void setPrice1(int price1) {
		synchronized (this) {
			System.out.println("ENTERED THE METHOD TO SET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			try {
				/**
				 * The sleep time of 2 seconds have been added intentionally and
				 * is less than that of READ lock(10S) to show that read lock
				 * and write lock can coexist and it is not like lock where if
				 * in one method if lock is acquired we wont be able to get lock
				 * in different method Thats not the case with read write lock .
				 * read lock is a different lock and write lock is a different
				 * lock Consider like these are two different objects used to
				 * lock critical section. We have added two set methods to show
				 * that write locks are within itself mutually exclusive
				 */
				Thread.currentThread().sleep(5000);
				this.price1 = price1;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				System.out.println("LEFT THE METHOD TO SET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			}
		}

	}

	public  int getPrice2() {
		synchronized (this) {
			System.out.println("ENTERED THE METHOD TO GET PRICE 2 BY THREAD " + Thread.currentThread().getName());
			try {
				/**
				 * The sleep time of 10 seconds have been added intentionally
				 * which is same as that of getPrice1 to show that read lock and
				 * write lock can coexist and it is not like lock where if in
				 * one method if lock is acquired we wont be able to get lock in
				 * different method Thats not the case with read write lock .
				 * read lock is a different lock and write lock is a different
				 * lock Consider like these are two different objects used to
				 * lock critical section
				 * 
				 * However Two getPrice method is uded to display that read LOCK
				 * is Mutually Exclusive
				 */
				Thread.currentThread().sleep(25000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				System.out.println("LEFT THE METHOD TO GET PRICE 2 BY THREAD " + Thread.currentThread().getName());
			}
			return price2;

		}
	}

	public  void setPrice2(int price2) {

		synchronized (this) {
			System.out.println("ENTERED THE METHOD TO SET PRICE 2 BY THREAD " + Thread.currentThread().getName());
			try {
				/**
				 * The sleep time of 2 seconds have been added intentionally and
				 * is less than that of READ lock(10S) to show that read lock
				 * and write lock can coexist and it is not like lock where if
				 * in one method if lock is acquired we wont be able to get lock
				 * in different method Thats not the case with read write lock .
				 * read lock is a different lock and write lock is a different
				 * lock Consider like these are two different objects used to
				 * lock critical section
				 */
				Thread.currentThread().sleep(5000);
				this.price2 = price2;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				System.out.println("LEFT THE METHOD TO GET PRICE 2 BY THREAD " + Thread.currentThread().getName());
			}
		}
	}

	int price1;
	int price2;

}