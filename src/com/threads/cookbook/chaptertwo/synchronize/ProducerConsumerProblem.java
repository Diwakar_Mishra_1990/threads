package com.threads.cookbook.chaptertwo.synchronize;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerProblem {
	public static void main(String[] args) {
		List<Integer> storageList = new ArrayList<Integer>();
		ResourceStorage lResource = new ResourceStorage(5, storageList);
		Producer producer = new Producer(lResource);
		Consumer consumer = new Consumer(lResource);
		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);
		producerThread.start();
		consumerThread.start();
	}
}

class ResourceStorage {
	private int maxSize;

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public List<Integer> getIntegerStore() {
		return integerStore;
	}

	public void setIntegerStore(List<Integer> integerStore) {
		this.integerStore = integerStore;
	}

	private List<Integer> integerStore;

	public ResourceStorage(int maxSize, List<Integer> integerStore) {
		// TODO Auto-generated constructor stub
		this.maxSize = maxSize;
		this.integerStore = integerStore;
	}

	// Wait and notify always called at synchronized block of code
	/**
	 * Java provides the wait(), notify(), and notifyAll() methods implemented
	 * in the Object class. A thread can call the wait() method inside a
	 * synchronized block of code. If it calls the wait() method outside a
	 * synchronized block of code, the JVM throws an
	 * IllegalMonitorStateException exception. When the thread calls the wait()
	 * method, the JVM puts the thread to sleep and releases the object that
	 * controls the synchronized block of code that it's executing and allows
	 * the other threads to execute other blocks of synchronized code protected
	 * by that object. To wake up the thread, you must call the notify() or
	 * notifyAll() method inside a block of code protected by the same object.
	 */
	public synchronized void consumeResource() {
		/**
		 * Implement the synchronized method get() to get an event for the
		 * storage. First, check if the storage has events or not. If it has no
		 * events, it calls the wait() method until the storage has some events.
		 * At the end of the method, we call the notifyAll() method to wake up
		 * all the threads that are sleeping in the wait() method.
		 */
		while (this.getIntegerStore().size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("PROBLEM WITH ConsumER");
			}
		}
		System.out.println(this.getIntegerStore().size() - 1 + " REMOVED");
		this.getIntegerStore().remove(this.getIntegerStore().size() - 1);
		notifyAll();
	}

	/**
	 * Java provides the wait(), notify(), and notifyAll() methods implemented
	 * in the Object class. A thread can call the wait() method inside a
	 * synchronized block of code. If it calls the wait() method outside a
	 * synchronized block of code, the JVM throws an
	 * IllegalMonitorStateException exception. When the thread calls the wait()
	 * method, the JVM puts the thread to sleep and releases the object that
	 * controls the synchronized block of code that it's executing and allows
	 * the other threads to execute other blocks of synchronized code protected
	 * by that object. To wake up the thread, you must call the notify() or
	 * notifyAll() method inside a block of code protected by the same object.
	 */
	public synchronized void produceResource(int resource) {
		/**
		 * Implement the synchronized method set() to store an event in the
		 * storage. First, check if the storage is full or not. If it's full, it
		 * calls the wait() method until the storage has empty space. At the end
		 * of the method, we call the notifyAll() method to wake up all the
		 * threads that are sleeping in the wait() method.
		 */
		while (this.getIntegerStore().size() == this.getMaxSize()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("PROBLEM WITH PRODUCER");
			}
		}
		this.getIntegerStore().add(resource);
		System.out.println(resource + " Added At Position " + (this.getIntegerStore().size() - 1));
		notifyAll();
	}

}

class Producer implements Runnable {
	ResourceStorage resourceStorage;

	public Producer(ResourceStorage resourceStorage) {
		// TODO Auto-generated constructor stub
		this.resourceStorage = resourceStorage;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			resourceStorage.produceResource(i);
		}
	}

}

class Consumer implements Runnable {
	ResourceStorage resourceStorage;

	public Consumer(ResourceStorage resourceStorage) {
		// TODO Auto-generated constructor stub
		this.resourceStorage = resourceStorage;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			resourceStorage.consumeResource();
		}
	}

}