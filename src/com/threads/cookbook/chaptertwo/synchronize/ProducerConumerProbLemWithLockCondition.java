package com.threads.cookbook.chaptertwo.synchronize;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Diwakar Mishra In this tutorial we will discuss about the Condition
 *         interface in Java. A Condition object, also known as condition
 *         variable, provides a thread with the ability to suspend its
 *         execution, until the condition is true. A Condition object is
 *         necessarily bound to a Lock and can be obtained using the
 *         newCondition() method. Furthermore, a Condition enables the effect of
 *         having multiple wait-sets per object, by combining these sets with
 *         the use of a Lock implementation. Moreover, due to the fact that
 *         Conditions access portions of state shared among different threads,
 *         the usage of a Lock is mandatory. It is important to mention that a
 *         Condition must atomically release the associated Lock and suspend the
 *         current�s thread execution.
 *         The structure of the Condition interface
Methods

await()
The current thread suspends its execution until it is signalled or interrupted.
await(long time, TimeUnit unit)
The current thread suspends its execution until it is signalled, interrupted, or the specified amount of time elapses.
awaitNanos(long nanosTimeout)
The current thread suspends its execution until it is signalled, interrupted, or the specified amount of time elapses.
awaitUninterruptibly()
The current thread suspends its execution until it is signalled (cannot be interrupted).
await(long time, TimeUnit unit)
The current thread suspends its execution until it is signalled, interrupted, or the specified deadline elapses.
signal()
This method wakes a thread waiting on this condition.
signalAll()
This method wakes all threads waiting on this condition.
 */
public class ProducerConumerProbLemWithLockCondition {

	public static void main(String[] args) {
		SharedResource lResource = new SharedResource(5);
		for(int i=0 ;i<10;i++){
			ProducerLock lProducer = new ProducerLock(lResource, i);
			Thread t1 = new Thread(lProducer,"PRODUCER-"+i);
			t1.start();
			ConsumerLock lConsumer = new ConsumerLock(lResource);
			Thread t2 = new Thread(lConsumer,"CONSUMER-"+i);
			t2.start();
		}
	}
}

class ProducerLock implements Runnable {

	SharedResource lResource;
	private int resourceInput;
	public ProducerLock(SharedResource lResource, int resourceInput) {
		// TODO Auto-generated constructor stub
		this.lResource= lResource;
		this.resourceInput = resourceInput;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.produce(this.resourceInput);
	}
	
}

class ConsumerLock implements Runnable {

	SharedResource lResource;
	public ConsumerLock(SharedResource lResource) {
		// TODO Auto-generated constructor stub
		this.lResource=lResource;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.consume();
	}
	
}
/**
 * 
 * @author Diwakar Mishra
 *Shared Resource  has a shared list .  It has a lock
 */
class SharedResource {
	int maxSize;
	List<Integer> sharerdList = new ArrayList<Integer>();
	Lock lReEntrantLock =new ReentrantLock();
	Condition lEmpty = lReEntrantLock.newCondition();
	Condition lFull = lReEntrantLock.newCondition();
	
	public SharedResource(int maxSize) {
		// TODO Auto-generated constructor stub
		this.maxSize = maxSize;
	}
	
	public void produce(int producedResource) {
		/**
		 * First we lock block of the code
		 * just as synchronized
		 */
		lReEntrantLock.lock();
		while (this.sharerdList.size() >= this.maxSize) {
			try {
				/**
				 * Similar to the wait here we use he condition to wait This is
				 * internal to the reentrant lock. This is to handle multiple
				 * producer as compared to multiple conumer If the list is full
				 * the thread which entered the thread waits for some time unti
				 * the queue is available to take any records . When the
				 * condition is signalled from consumer thread . then this block
				 * of code is executed
				 */
				lFull.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("PRODUCE PERFORMED BY THREAD :"+Thread.currentThread().getName());
		this.sharerdList.add(producedResource);
		/**
		 * In case if my queue is empty and my consumer thread is waiting for
		 * element to be added in the queue . then we must signal the consumer.
		 * This is similar to notifyall except it is associated with lock
		 */
		System.out.println(this.sharerdList);
		System.out.println("PRODUCTION COMPLETED BY THREAD :"+Thread.currentThread().getName());
		lEmpty.signalAll();
		lReEntrantLock.unlock();
	}

	public void consume() {
		/**
		 * First we lock block of the code
		 * just as synchronized
		 */
		lReEntrantLock.lock();
		System.out.println(this.sharerdList.size()+"<=============>"+Thread.currentThread().getName());
		while (this.sharerdList.size() == 0) {
			try {
				/**
				 * Similar to the wait here we use he condition to wait This is
				 * internal to the reentrant lock. This is to handle multiple
				 * consumer as compared to multiple producer If the list is
				 * empty the thread which entered the thread waits for some time
				 * until some elements are added in the queue . When the
				 * condition is signalled from producer thread . then this block
				 * of code is executed
				 */
				System.out.println("INSIDE EMPTY");
				lEmpty.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("CONSUME PERFORMED BY THREAD :"+Thread.currentThread().getName());
		System.out.println(this.sharerdList);
		this.sharerdList.remove(this.sharerdList.size()-1);
		System.out.println("CONSUMPTION COMPLETED BY THREAD :"+Thread.currentThread().getName());
		lFull.signalAll();
		lReEntrantLock.unlock();
	}
}