package com.threads.cookbook.chaptertwo.synchronize;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import oracle.jrockit.jfr.events.RequestableEventEnvironment;

/**
 * @author Diwakar Mishra
 *
 */
/**
 * Java provides another mechanism for the synchronization of blocks of code.
 * It's a more powerful and flexible mechanism than the synchronized keyword.
 * It's based on the Lock interface and classes that implement it (as
 * ReentrantLock). This mechanism presents some advantages, which are as
 * follows:
 * It allows the structuring of synchronized blocks in a more
 * flexible way. With the synchronized keyword, you have to get and free the
 * control over a synchronized block of code in a structured way. The Lock
 * interfaces allow you to get more complex structures to implement your
 * critical section. 
 * The Lock interfaces provide additional functionalities
 * over the synchronized keyword. One of the new functionalities is implemented
 * by the tryLock() method. This method tries to get the control of the lock and
 * if it can't, because it's used by other thread, it returns the lock. With the
 * synchronized keyword, when a thread (A) tries to execute a synchronized block
 * of code, if there is another thread (B) executing it, the thread (A) is
 * suspended until the thread (B) finishes the execution of the synchronized
 * block. With locks, you can execute the tryLock() method. This method returns
 * a Boolean value indicating if there is another thread running the code
 * protected by this lock. 
 * The Lock interfaces allow a separation of read and
 * write operations having multiple readers and only one modifier. ff The Lock
 * interfaces offer better performance than the synchronized keyword. In this
 * recipe, you will learn how to use locks to synchronize a block of code and
 * create a critical section using the Lock interface and the ReentrantLock
 * class that implements it, implementing a program that simulates a print
 * queue.
 *
 */
public class SynchronizeBlockWithLock {
	public static void main(String[] args) {
     PrintQueue lQueue = new PrintQueue();
     for(int i=0;i<10;i++) {
    	 PrintJoB lJoB = new PrintJoB(lQueue);
    	 Thread t= new Thread(lJoB, " PRINT#-#"+i);
    	 t.start();
     }
	}
}
/**
 * 
 * @author Diwakar Mishra
 * Here we are implementing a printer
 */
class PrintQueue {
	/**
	 * Declare a Lock object and initialize it with a new object of the
	 * ReentrantLock class.
	 */
	Lock printLock = new ReentrantLock();

	/**
	 * This method synchronizes the flow of the print action
	 */
	public void printJob() {

		/**
		 * We expect the otput to be regular for Each thread
		 */
		try {
			printLock.lock();
			System.out.println("########################   START   ########################");
			System.out.println("INITIALIZING PRINT JOB OF "+Thread.currentThread().getName());
			System.out.println("PRINTING PRINT JOB OF "+Thread.currentThread().getName());
			System.out.println("PRINT JOB OVER "+Thread.currentThread().getName());
			System.out.println("EXITING THREAD "+Thread.currentThread().getName());
			System.out.println("######################### END  #######################");
		}catch(Exception e) {
			System.out.println("Exception caught");
		}
		finally {
			printLock.unlock();
		}
	}
}

class PrintJoB implements Runnable {
	PrintQueue mPrintQueue;
	public PrintJoB(PrintQueue mPrintQueue) {
		this.mPrintQueue = mPrintQueue;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		mPrintQueue.printJob();
	}
	
}