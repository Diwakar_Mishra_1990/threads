package com.threads.cookbook.chaptertwo.synchronize;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 
 * @author Diwakar Mishra 
 */
/**
 * 
 * 
 * A ReadWriteLock maintains a pair of associated locks, one for read-only
 * operations and one for writing. The read lock may be held simultaneously by
 * multiple reader threads, so long as there are no writers. The write lock is
 * exclusive. All ReadWriteLock implementations must guarantee that the memory
 * synchronization effects of writeLock operations (as specified in the Lock
 * interface) also hold with respect to the associated readLock. That is, a
 * thread successfully acquiring the read lock will see all updates made upon
 * previous release of the write lock.
 * 
 * A read-write lock allows for a greater level of concurrency in accessing
 * shared data than that permitted by a mutual exclusion lock. It exploits the
 * fact that while only a single thread at a time (a writer thread) can modify
 * the shared data, in many cases any number of threads can concurrently read
 * the data (hence reader threads). In theory, the increase in concurrency
 * permitted by the use of a read-write lock will lead to performance
 * improvements over the use of a mutual exclusion lock. In practice this
 * increase in concurrency will only be fully realized on a multi-processor, and
 * then only if the access patterns for the shared data are suitable.
 * 
 * Whether or not a read-write lock will improve performance over the use of a
 * mutual exclusion lock depends on the frequency that the data is read compared
 * to being modified, the duration of the read and write operations, and the
 * contention for the data - that is, the number of threads that will try to
 * read or write the data at the same time. For example, a collection that is
 * initially populated with data and thereafter infrequently modified, while
 * being frequently searched (such as a directory of some kind) is an ideal
 * candidate for the use of a read-write lock. However, if updates become
 * frequent then the data spends most of its time being exclusively locked and
 * there is little, if any increase in concurrency. Further, if the read
 * operations are too short the overhead of the read-write lock implementation
 * (which is inherently more complex than a mutual exclusion lock) can dominate
 * the execution cost, particularly as many read-write lock implementations
 * still serialize all threads through a small section of code. Ultimately, only
 * profiling and measurement will establish whether the use of a read-write lock
 * is suitable for your application.
 * 
 * Although the basic operation of a read-write lock is straight-forward, there
 * are many policy decisions that an implementation must make, which may affect
 * the effectiveness of the read-write lock in a given application. Examples of
 * these policies include:
 * 
 * Determining whether to grant the read lock or the write lock, when both
 * readers and writers are waiting, at the time that a writer releases the write
 * lock. Writer preference is common, as writes are expected to be short and
 * infrequent. Reader preference is less common as it can lead to lengthy delays
 * for a write if the readers are frequent and long-lived as expected. Fair, or
 * "in-order" implementations are also possible. Determining whether readers
 * that request the read lock while a reader is active and a writer is waiting,
 * are granted the read lock. Preference to the reader can delay the writer
 * indefinitely, while preference to the writer can reduce the potential for
 * concurrency. Determining whether the locks are reentrant: can a thread with
 * the write lock reacquire it? Can it acquire a read lock while holding the
 * write lock? Is the read lock itself reentrant? Can the write lock be
 * downgraded to a read lock without allowing an intervening writer? Can a read
 * lock be upgraded to a write lock, in preference to other waiting readers or
 * writers? You should consider all of these things when evaluating the
 * suitability of a given implementation for your application.
 *
 */
public class SynchronizeDataAccessWithReadWriteLock {
	public static void main(String[] args) {
		ReadWriteResource lResource = new ReadWriteResource();
		for (int i = 0; i < 10; i++) {
			GetPrice1Runnnable lGetPrice1Runnnable = new GetPrice1Runnnable(lResource);
			Thread t1 = new Thread(lGetPrice1Runnnable, "THREAD-GET_PRICE_1-" + i);
			SetPrice1Runnnable lSetPrice1Runnnable = new SetPrice1Runnnable(lResource, i);
			Thread t3 = new Thread(lSetPrice1Runnnable, "THREAD-SET_PRICE_1-" + i);
			t1.start();
			/**
			 * If we comment this line we will be able to seet hat all the
			 * readers get access to the critical section simultaneously. If we
			 * uncomment this line then we will come to know that write operaion
			 * is eclusive and only one write operation can be performed in the
			 * critical section . At that time all the reader must wait
			 */
			t3.start();
		}
	}
}

class GetPrice1Runnnable implements Runnable {
	ReadWriteResource lResource;

	public GetPrice1Runnnable(ReadWriteResource lResource) {
		// TODO Auto-generated constructor stub
		this.lResource = lResource;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.getPrice1();
	}

}

class SetPrice1Runnnable implements Runnable {
	ReadWriteResource lResource;
	int price;

	public SetPrice1Runnnable(ReadWriteResource lResource, int price) {
		// TODO Auto-generated constructor stub
		this.price = price;
		this.lResource = lResource;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		lResource.setPrice1(this.price);
	}

}

class ReadWriteResource {
	/**
	 * This is all together a different issue here Here we are using
	 * ReadWriteLock not Lock which is different
	 */

	ReadWriteLock lReentrantReadWriteLock = new ReentrantReadWriteLock();

	public int getPrice1() {
		lReentrantReadWriteLock.readLock().lock();
		System.out.println("ENTERED THE METHOD TO GET PRICE 1 BY THREAD " + Thread.currentThread().getName());
		try {
			/**
			 * The sleep time of 10 seconds have been added intentionally to
			 * show that multiple read lock can exist until a write lock takes
			 * takes control of this critical section if lock is acquired by
			 * read we will be able to get lock if our thread intends to do a
			 * read
			 */
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("LEFT THE METHOD TO GET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			lReentrantReadWriteLock.readLock().unlock();
		}
		return price1;
	}

	public void setPrice1(int price1) {

		lReentrantReadWriteLock.writeLock().lock();
		System.out.println("ENTERED THE METHOD TO SET PRICE 1 BY THREAD " + Thread.currentThread().getName());
		try {
			/**
			 * The sleep time of 2 seconds have been added intentionally and is
			 * less than that of READ lock(10S) to show that read lock and write
			 * In case if write lock enters the critical section no read can
			 * take place If write acquired a lock then no read enters the
			 * critical section and no write enters the critical section
			 */
			Thread.currentThread().sleep(1000);
			this.price1 = price1;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("LEFT THE METHOD TO SET PRICE 1 BY THREAD " + Thread.currentThread().getName());
			lReentrantReadWriteLock.writeLock().unlock();
		}
	}

	int price1;
	int price2;

}